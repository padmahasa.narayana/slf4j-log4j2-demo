package net.theoreminc;

/**
 * 
 */
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author padmahasa.narayana
 *
 */
public class LoggerDemo {

	/**
	 * @param args
	 */
	final Logger logger10 = LoggerFactory.getLogger(this.getClass());
	Integer t;
	Integer oldT;

	public static void main(String[] args) {
		LoggerDemo logDemo = new LoggerDemo();
		logDemo.setTemperature(18);
	}

	public void setTemperature(Integer temperature) {
		oldT = t;
		t = temperature;

		logger10.debug("Temperature set to {}. Old temperature was {}.", t, oldT);

		if (temperature.intValue() > 50) {
			logger10.info("Temperature has risen above 50 degrees.");
		}
	}
}
